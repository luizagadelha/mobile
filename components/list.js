import React, { useState, useEffect } from "react";
import {
  View,
  FlatList,
  StyleSheet,
  Text,
} from "react-native";
import api from "../api";

export default function Lista() {
  const [incidents, setIncidests] = useState([]);

  const getItems = async () => {
    await api.get("find").then((res) => {
      setIncidests(res.data);
    });
  };

  useEffect(() => {
    getItems();
  }, []);

  function TextoTabela({ marca, quantidade, preco, data }) {
    const dia = [data[8], data[9]];
    const mes = [data[5], data[6]];
    const ano = [data[0], data[1], data[2], data[3]];
    return (
      <View style={styles.container}>
        <Text style={styles.texto}>Marca: {marca}</Text>
        <Text style={styles.texto}>Quant.(kg): {quantidade}</Text>
        <Text style={styles.texto}>Preço(R$): {preco}</Text>
        <Text style={styles.texto}>Data: {dia}/{mes}/{ano}</Text>
      </View>
    );
  }

  return (
    < View style={{ justifyContent: 'center', backgroundColor: "#2b2b6f", flex:1 }}>
      <Text style={styles.titulo}>Visualizar Compras</Text>
      <FlatList
        numColumns={2}
        data={incidents}
        keyExtractor={(item) => item._id}
        renderItem={({ item }) => (
          <TextoTabela
            marca={item.brand}
            quantidade={item.quantity}
            preco={item.price}
            data={item.createdAt}
          />
        )}
      />
      </View>    
  );
}

const styles = StyleSheet.create({
  titulo: {
    fontSize: 25,
    fontWeight: "bold",
    color: "#fff",
    marginTop: 25,
    marginBottom: 20,
    alignSelf: "center",
  }, 

  container: {
    flex: 1,
    backgroundColor: "#fff",
    justifyContent: "space-evenly",
    alignItems: "center",
    borderRadius: 15,
    margin: 10,
    width: 70,
    paddingBottom: 10,
    paddingTop: 10,
  },

  texto:{
    fontSize: 20,
    justifyContent: "space-evenly",
    color: "#2b2b6f",
  },
});
