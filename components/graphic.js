import React,  { useState, useEffect } from "react";
import {View, Text, Dimensions} from "react-native";
import * as Svg from 'react-native-svg';
import api from "../api";
import { VictoryChart, VictoryGroup, VictoryBar, VictoryLegend} from "victory-native"

export default function Graphic(){

    const data = {
        teste:[
            {x:"Week 1", y:"20"},
            {x:"Week 2", y:"30"}
        ],
        teste2:[
            {x:"Week 1", y:"50"},
            {x:"Week 2", y:"80"}
        ],
    }

    const [incidents, setIncidents] = useState([]);
   // const [test, setTest] = useState([]);

    const getItems = async () => {
    await api.get("find").then((res) => {
        let inc = res.data;
        let arrAux = []

        inc.forEach(a => {
            arrAux.push(a)
        });

        let quantidade = []

        arrAux.forEach(index => {
            //console.log(index.quantity)
            quantidade.push(index.quantity)
        })
        console.log(quantidade);
    });
  };

    useEffect(() => {
        getItems();
    }, []);

    return (
        <View>
            <VictoryChart>
                <VictoryGroup offset={30}>
                    <VictoryBar data={data.teste} style={{
                        data: {
                            fill: "lightblue"
                        }
                    }}/>
                    <VictoryBar data={data.teste2} style={{
                        data: {
                            fill:"darkblue"
                        }
                    }}/>                    
                </VictoryGroup>
                <VictoryLegend 
                    x={Dimensions.get('screen').width/2 - 80}
                    orientation="horizontal"
                    gutter={20}
                    data={[
                        {
                            name: 'teste',
                            symbol: {
                                fill:'lightblue'
                            }
                        },
                        {
                            name: 'teste2',
                            symbol: {
                                fill:'darkblue'
                            }
                        }
                    ]}/>
            </VictoryChart>
        </View>
    )
}