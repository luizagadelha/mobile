import React from "react";
import {View, Text, StyleSheet} from "react-native";
import Grafico from "./grafico";

export default function GraficoTitulo(){
return(
    <View >
    <View style={{alignContent:'center', justifyContent:'center'}}>
        <Text style={styles.titulo}>Gráfico das Quantidades Compradas</Text>
        <Grafico/>
    </View>
    </View>
)
}
const styles = StyleSheet.create({
    titulo: {
        fontSize: 25,
        fontWeight: "bold",
        color: "#2b2b6f",
        marginTop: 25,
        marginBottom: 20,
        alignSelf: "center",
    }, 
});