import React, {useEffect, useState} from "react";
import {View, StyleSheet} from "react-native";
import {Text} from "react-native-svg";
import { PieChart } from "react-native-svg-charts"
import api from "../api";

export default function Grafico() {

    const [incidents, setIncidests] = useState([]);

    const getItems = async () => {
        await api.get("find").then((res) => {
          setIncidests(res.data);
        });
      };
    
        useEffect(() => {
            getItems();
        }, []);


    const quantidade = [];

    incidents.forEach(index => {
            quantidade.push(index.quantity)
    })

    const data = quantidade;
    const pieData = data.map((value, index) => ({
        value, 
        key: `${index}-${value}`,
        svg: {
            fill:'#2b2b6f'
        }
    }));

    const Label = ({ slices }) => {
        return slices.map((slice, index) => {
            const {pieCentroid, data} = slice;
            return(
                <Text
                    key={`label-${index}`}
                    x = {pieCentroid[0]}
                    y = {pieCentroid[1]}
                    fill="white"
                    textAnchor={'middle'}
                    alignmentBaseline={'middle'}
                    fontSize={18}
                >
                    {`${data.value} kg`}
                </Text>
            )
        })
    }

    return (
        <View>
            <PieChart style={{height:200}} data={pieData}>
                <Label/>
            </PieChart>

        </View>
    );
}
